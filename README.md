# my tech/code blog website.

~~powered by Hugo~~

hosted elsewhere by Caddy

but the `CNAME` in this repo, means old links to github pages redirect

## Next.js

I want to use next JS for this, but also want it to be an static generated site.
Next.js does static site export, but actually we really need another step that does
code-gen for next to produce a site with. Nothing does this (well enough), so here is
my proposal.

We need an index, so we can render/paginate/sort the list of posts. This means, pre-process
the posts and create the index.

I found a nice markdown babel loader that can generate a react component from markdown. given the
preprocessing step I already use, we probably don't need the babel-loader, just the code transform
behind it and save that into our "pages" directory.
