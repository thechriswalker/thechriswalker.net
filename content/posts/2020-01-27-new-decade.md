+++
title = "A New Decade"
date = "2020-01-27"
+++

So... I didn't manage to write at all last year. I was busy. That's no real excuse and I do quite like writing these posts.

What did I do last year?

- I _didn't_ work on [OPFS](https://gitlab.com/opfs) at all :(
- I did release a bunch of code for writing network server software in TypeScript: https://github.com/thechriswalker/proc (which I have been dogfooding)
- I wrote a tool to scrape and archive the publicly available data from Parkruns (and ran a few myself), with a view to visualising the data, and making it easier to explore.
- I did 2 more exams on the way to completing a masters in Information Security.
- I started a bunch more projects (that may or may not ever complete):
  - `SNAC` a Network Access Controller I can run on my network to collect bandwidth stats and lock down what individual devices can do. This is good fun as it runs a netfilter queue to decide what to do with packets and interacts with `iptables` which is interesting stuff.
  - `BookThing` a book shelf manager / reading log tool (this is where the `proc` dogfooding is happening).
  - `CharityThing` which was a good idea, but I think will have to be ditched. The plan was to create a tool which you could use to budget a recurring amount of money to donate to charity and the tool would split it between your preferred charities every month. However there is no easy way to programmatically give money to a charity on behalf of an individual. Each charity does it in it's own way and usually requires manual steps. So this project is dead in the water - charity onboarding is not something I want to spend a lot of time doing.
  - A TypeScript Labyrinth game: I played the board game and liked it and decided to make a game engine for it. I built a TUI for the engine and would like to build a GUI.

And what do I have planned for this year?

I would really like to get `SNAC` in a good place, it has been kicking around for a while and I'd love to have it's functionality running on my local network.
