+++
title = "> 1 year later, OPFS update"
date = "2014-12-04"
+++
So OPFS. I have now been through 3 iterations of UI, and a complete rewrite of the backend.

I am actually quite proud of the backend now, it was a mess before. Also, I implemented my own indexing and search system so no more reliance on Elasticsearch (not that ES isn't great, I just want dependency free!). So I learnt a bunch. 

> Most importantly, just *do*. 

I wasn't sure about the search index, I thought it would be too complex, too much memory, too slow, etc... but I just built the simplest thing I could think of to solve the problem. Everything in memory, search traverses the entire corpus. Seems awful, but actually for the order of magnitude of documents I am dealing with (ten's of thousands) it performs superbly, and the memory usage is well within reason.

So, OPFS has a new http API which should be easier to use and the codebase is far less complex. Awesomes. But it does desperately need the UI finishing. That is a long way off... maybe by next December...

Also, I moved the codebase to Gitlab for no good reason so there is:

https://gitlab.com/opfs/opfs-server
https://gitlab.com/opfs/opfs-ui

My next *impossible* feature is face recognition (not just detection, but actual recognition).
