+++
title = "ReactJS and the Flux Pattern"
date = "2014-09-19"
+++
ReactJS is a JavaScript library from Facebook. It turns web application development
completely around. If you asked me 6 months ago if I'd ever write web applications
without jQuery (or one of the alternatives) I would have thought the answer an obvious "No".

I still think the answer is obvious, but it's no longer "No".

Using React means that you don't ever have to manipulate the DOM.

Yes, that's what I said. Instead, you tell React how you want the DOM to look at
any given state of your application and React worries about any DOM manipulations
that may or may not be necessary. Technically, React creates a fake, virtual DOM
in javascript and re-creates it whenever your state changes. It then uses a diff
algorithm to compute what needs to change to go from previous to current. Then it
applies the change set to the real DOM.

The beauty is that the DOM is slow and JavaScript is is fast, so this process
minimises interaction with the slow DOM.

A simple example: a Pagination indicator.

```javascript 
var Paginator = React.createClass({
  render: function(){
      if(this.props.loading){
          return LoadingIndicator();
      }
      if(this.props.maxReached){
          return null;
      }
      return Button({
          type: "default",
          text: "Load More...",
          icon: "chevron-down",
          onClick: this.props.paginate
      });
  }
});
```

This does a few things depending on the `props` given to it. It can show a
loading indicator if we are currently loading the next data, or it can be blank
if we have reached the end, or it can be a button which calls a function on click.

As you can see we define no transition between these states, it all comes from
the declarative code here.

Of course, this model raises it's own challenges. Namely how to control the `state`
and what data flows to the components of your application.  Which is where the
"/flux pattern/" comes in.

The Flux Pattern is another thing to come out of Facebook, and it basically describes
a uni-directional flow of information around you app. That is all data comes in
from the top, flows through the application and if anything wants to trigger a
change, it "/dispatches/" it back up to the top, where registered handlers listen
for the dispatch, and perform some task (or tasks), maybe update some data, and
if so tell the app there is new data, which causes a re-render with data flowing
down the components (and, of course, only anything that has changed is re-written to
the DOM).

There's a bit more to Flux -- ordering of dispatching, the `stores`, etc... -- but
that main idea is the uni-driectional data flow.

It's complex to master but has allowed us to do some very cool things with the application.
Firstly, keeping complex data in sync is now a no-brainer. Change data in one place,
and anywhere in the app that uses that data will just update -- magically.  Another
useful ability is to share the javascript code for the client with a NodeJS process
on the server and pre-render the HTML in these pages, single page javascript app
with pre-rendering means fast initial load and good SEO possibilities.

Having used React for ~6 months now, I can't imagine writing a complex web app without it.
