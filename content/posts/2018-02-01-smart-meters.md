+++
title = "Smart Meters and data ownership"
date = "2018-02-01"
+++
I got a smart meter installed today, by npower.

One of the first questions I asked was, how do I get access to the data.

The engineer doing the installation was not sure what data I would have access to,
and interestingly mentioned that the meter he was installing is a *phase 1* meter
and likely it would need to be replaced with a *phase 2* one in a few months. I
mention this because it will be relevant.

So anyway, it gets installed and the In Home Display (`IHD` - everything has an acryonym...) shows my current usage. But that's not really useful, even with it's
shitty little graphs which will get some data eventually. I want to have that data
in a file or database (or spreadsheet!) that I can play with.

I want to be able to take my energy use and tariff price over time and store that. Then
draw a graph with other providers costs to analyse whether I made the best choice
or not (*spoiler alert* I probably didn't).

Anyway, a quick DuckDuckGo later and I find that these *phases* are probably the
standards `SMETS1` and `SMETS2`. Both run a `HAN` (Home Area/Automation Network)
and the Smart meter acts as the hub. The `IHD` connects to the hub and queries it
for the data to display. Now the thing about this is that the `HAN` is run over *ZigBee*
which is the same system that Phillips Hue lightbulbs and a bunch of other IoT stuff
uses. So can we just get a ZigBee antenna and do the same? Well no.

1. **The Smart Meter Hub will not allow arbitrary pairing into the HAN.** This is speculation on my part, but I got that impression from the way my IHD was set up.
2. **The communication between the IHD and Meter is encrypted** Using a preconfigured
public key encryption scheme so the resultant key is never sent in the clear and the IHD
must *know* the secret already. (which means, yes, some sort of single master key...)

So first we need to pair to the meter, then figure our the encryption key to query it.

But, this is the case for the `SMETS1` system that I currently have. In a `SMETS2`
situation, there is provision for a `CAD` (Consumer Access Device) to sit on the edge
of the Smart Meter's `HAN` and allow access to data from the secure enclave.

This is what I want. But I can't have it yet, unless I cheat and set up a Raspberry Pi
to *literally* watch the smart meter 24<span>/</span>7 and OCR the usage values every few seconds, that or watch it myself and write down the values...

**Technically** I do own the data, but it's majorly inconvenient.

Not sure I have the time for that. So we wait for `SMETS2`...
