+++
title = "Zero.js - jQuery alternative?"
date = "2012-12-04"
+++
`jQuery` **is** the defacto standard when it comes to all-purpose browser-based javascript libraries.
There is no doubt unto it's awesomeness.

However, it has been remarked upon as getting bloated and so there was born a project called `jquip`
which aimed to reduce bulk by modularising the code and allowing custom builds that had only what was
needed in them. `jQuery` themselves have now provided this same functionality and everything is better
off for it.

Also `jQuery` is dropping support for some legacy IE versions and so that will cut out a lot of code
that was rarely ever used. Again, that will be a massive win against bloat.

However, `jQuery` is the kitchen sink - or at least everything but the kitchen sink. Given that `VanillaJS`
is so powerfull anyway, I have wondered for a long time if the main utility of `jQuery` could be reproduced
in a small library. If I discard any browser compatilibility then we can make it even smaller. So `Zero.js`
was born.

`Zero.js` is a lightweight library that does a small amount of what `jQuery` does but in a tiny package. I
have included only the following:

  - $(selector, context?) -- selectors like querySelector, no `:visible`, etc...
  - $(html)
  - $(DOMElement)
  - $(NodeList)
  - .find(selector)
  - .get(index)
  - .length
  - .each(fn)
  - .add/remove/has -Class
  - .attr(name, value?)
  - .data(name, value?)
  - .css(attrObj)
  - .css(attr, val)
  - $.ajax -- which functions very differently to `jQuery`'s
  - $.extend(obj, obj, ...?)

I'd only think of using inside a controlled environment - e.g. personal tools - as it uses [WeakMaps](//link?)
which aren't even in the latest Chrome unless and `chrome:flags` switch is switched...
