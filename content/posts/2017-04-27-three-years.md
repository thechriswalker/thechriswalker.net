+++
title = "It's been 3 years..." 
date = "2017-04-27"
+++
It's my daughter's birthday today. She is now 3 years old. 

### Un - Be - Lieveable.

So **Happy Birthday Florence!** It's crazy you are 3 already and such a gorgeous little girl. I love you.

But I realised today that I started `OPFS` 3 years ago too, just before you was born.

`OPFS` is my vision for archiving my photos/videos in a 
cloud-independent way.

I love Google Photos and have all my photos there. But I 
don't want that as the source of truth. I want to backup
a copy. Of course this can be done easily, but then I just
have a big disk full of photos. Again not too much of an
issue, unless you want to find anything.

So enter `OPFS`, with it the vision that I will be able to
have all my photos in google photos, but also keep synchronized
a local copy indexed by `OPFS`, content-addressable so it's dedup'd 
(and I can re-import with no fear of duplication). Format is less
condusive to searching by hand, but the machine readable indexes
(that is, JSON files) could be indexed/searched easily by many
FOSS.

`OPFS` stores its data (the images/videos) in a content-addressable
blob store, the index data in another (or the same one). Both of these
store can be sync'd with any tool, FOSS or otherwise (I recommend 
[`syncthing`](https://syncthing.net/)).

Because I use Google Photos I intend to implement a sync daemon that
downloads/imports new stuff and also checks/creates `Collections` - read `Albums`
that match the Google Photos albums.

I also want to be able to connect multiple google accounts, because Rosie's photos
should be there too.

I have implemented Google Account OAuth for it, so Photos are private by default
but can be made public if wanted, or shared with specific people via a shared collection.

Most of the backend functionality is built, the UI is a major WIP, because I keep getting 
distracted by new tech...

But shit. 3 years I have been working on this. I should have something to show for it by now.

Maybe in another year or two.




