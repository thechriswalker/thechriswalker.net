+++
title = "Facebook 2-factor Auth with Google Authenticator"
date = "2012-10-29"
+++
I found out recently that Facebook supports 2-factor auth, which I'm a
massive fan of. However, there are 2 drawbacks:

  1. They always send you an SMS message with the login code, even if you
     choose to use an app to generate them.
  2. They claim that in order to use a mobile app (on Android) for your code,
     you have to install the facebook Android App.

The first one is a pain, as I get unneccesary SMS messages.

The second one is a lie, and any compliant TOTP generator will work. Like the
Google Authenticator App that I already use for my Google Accounts, AWS, etc...

However, provided you tell the facebook dialogs that you are struggling, they
give you the infomation you want to set it up.



