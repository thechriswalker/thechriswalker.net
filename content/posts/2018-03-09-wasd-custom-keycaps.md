+++
title = "WASD Custom Keycaps"
date = "2018-03-09"
+++
I treated myself.

I have been eyeing WASD keyboards for ages. But the price, with shipping to the UK and
import duty was a bit silly. I cracked though, made my layout, chose my colours and...

{{< img alt="Keycaps in place" src="/images/wasd.jpg" >}}

[Full album with more pics](https://photos.app.goo.gl/ddQgZ1DMPTO1VnwZ2) on Google Photos.

Lots of people talk about how bad the WASD keycaps are. But you simply
cannot buy a totally custom keycap set like this anywhere else. The things I customised specifically, apart from the colours, were:

* I added small numerals to the number row (that one I always miss when touchtyping).
* The modifier keys all have custom text, the spacebar has the `thechriswalker` branding.
* The the number pad is all binary and psuedo assembly instructions (`div`, `mul`, `ret`...)
* The Esc key has the glider on it.
* `HJKL` have arrows on them.

Of course I did get some things wrong... I would change a couple of things.

The grey on black was *too* low contrast. I wanted low, but this was too low, a slightly
lighter grey would have been better. Same for the orange chevrons on the `HJKL` keys, the
orange is too similar to the key color, would have looked better in dark grey.

The numpad was a mistake. I decided the grey keys would look nice, but I didn't change
the color of the labels in the layout SVG. So they ended up with the text the same color
as the keys. That's *way* to little contrast.

Otherwise I am happy and those little niggles don't detract from how awesome I think
the keyboard looks now.

Maybe it is about time I fixed my Ergodox and started using that...
