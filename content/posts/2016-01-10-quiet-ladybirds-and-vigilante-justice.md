+++
title = "Quiet Ladybirds and Vigilante Justice"
date = "2016-10-10"
+++
OK, so I have 2 kids and both myself and they love the Julia Donaldson books.

Orson, my eldest is a massive `The Snail and The Whale` fan and Flo loves `Stick Man`.

We recently got a copy of `What the Ladybird Heard Next` and despite sequels almost never being as good as the original, it was still good. However a recent
reading highlighted something I didn't notice first time around.

For those of you unfamiliar with the books, they revolve around a farm
with various animals, including a ladybird (who hardly ever spoke a word).
Then there are 2 bad guys, Hefty Hugh and Lanky Len. In the first book,
the bad guys try to steal the fine prize cow, but are thwarted and taken
away by the police. In the second, they have been released from prison and
are stealing eggs from the farm. They are thwarted again and this time they
are chased off by the bees from the beehive.

The important point here is that after being chased off by the bees, they
**never come back again**.

So let's recap, 

  1. bad guys dealt with by police and sent to prison => they get out and continue doing bad things.
  2. bad guys dealt with by locals and have stinging violence inflicted upon them => they are never seen again.

I am sure there is a moral there somewhere.
