+++
title = "On Android Development"
date = "2013-08-01"
+++
I never used Java before. I had visions of dependency hell and having to use a giant
IDE instead of Sublime.

Turns out Android Development isn't all that bad. We chose to use Maven as the dependency
and build tool - not sure if Android's new `Gradle` based system is any better, but
Maven is working for us.

Same situation with the IDE, I've been using Android Studio and despite:
  - having a splash page beacuse it...
  - takes 30-40 seconds to even start up and then
  - is laggy quite often

The benefits of the _smart_ environment are pretty big, especially for a beginner
like me, who is just learning all the API's I need to create apps.

Personally I found the whole `Activity`, `Service`, `Broadcast Reciever` and associated
lifecycles more complex than the new language. Java itself is pretty easy to grok.

It's been fun over the last few weeks getting familiar with what Android apps can do
(hint: *a _lot_*). Looking forward to writing something little for myself soon...
